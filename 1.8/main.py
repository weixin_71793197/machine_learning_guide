# 读取数据
import pandas as pd
data = pd.read_csv('Logistic_Regression_Data.csv')

# 给x和y赋值
x = data.drop(['success_or_fail'], axis=1)
y = data.loc[:,'success_or_fail']

# 训练模型
from sklearn.linear_model import LogisticRegression
model = LogisticRegression()
model.fit(x, y)

# 获取决策边界
theta1, theta2 = model.coef_[0]
theta0 = model.intercept_[0]

print(f"Decision Boundary: y = {theta1}x1 + {theta2}x2 + {theta0}")

# 获取预测值
prediction = model.predict(x)

# 计算准确率
from sklearn.metrics import accuracy_score
accuracy = accuracy_score(y, prediction)
print(f"Accuracy: {accuracy}")