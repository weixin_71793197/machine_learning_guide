# 导入数据
import pandas as pd
data = pd.read_csv('error_detection_data.csv')

# 训练异常检测模型
x = data.drop('label', axis=1)
from sklearn.covariance import EllipticEnvelope
model = EllipticEnvelope()
model.fit(x)

# 可视化异常数据
import matplotlib.pyplot as plt
plt.scatter(x['x1'], x['x2'], c=model.predict(x))
plt.show()

# 计算正确率
y = data.loc[:,'label']
print(model.score(x, y))

