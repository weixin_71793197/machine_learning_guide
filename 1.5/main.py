# 读取数据
import pandas as pd
data = pd.read_csv("USA_Housing.csv")

# 给x_multi和y赋值
x_multi = data.drop(["Price", "Address"], axis=1)
y = data.loc[:, "Price"]

# 训练模型
from sklearn.linear_model import LinearRegression
LR_multi = LinearRegression()
LR_multi.fit(x_multi, y)

# 计算预测值
y_predict = LR_multi.predict(x_multi)

# 具体值的转化
import numpy as np
x_test = [65000, 5, 5, 3, 30000]
x_test = np.array(x_test).reshape(1, -1)

# 用训练好的模型预测
y_test_predict = LR_multi.predict(x_test)
print(y_test_predict)


